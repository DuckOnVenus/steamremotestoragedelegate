import sys
import platform
import time

from Utils.TextFileWriter import TextFileWriter

if cmp(platform.system(), 'Windows') is 0:
    from Utils.WindowsConsoleWriter import WindowsConsoleWriter as PlatformConsoleWriter
else:
    from Utils.UnixConsoleWriter import UnixConsoleWriter as PlatformConsoleWriter

VERBOSE_LEVEL = 0


class Logger:
    __TIME_LABEL_PATTERN = u'%m/%d/%y-%H:%M:%S'
    __console_writer = PlatformConsoleWriter()
    __file_logger = None
    __LINE_PATTERN = u"[%s] <%s>\t%s\n"

    @staticmethod
    def init_logger(verbose_level, write_file, suffix="", log_file_dir="./log"):
        global VERBOSE_LEVEL
        VERBOSE_LEVEL = verbose_level
        if write_file:
            Logger.__file_logger = TextFileWriter(suffix, log_file_dir)

    def __init__(self):
        raise NotImplementedError(u"This class should never be instantiated.")

    @staticmethod
    def __gen_log_line(str_level, text):
        log_line = Logger.__LINE_PATTERN % (time.strftime(Logger.__TIME_LABEL_PATTERN, time.localtime(time.time())),
                                            str_level, text)
        return log_line

    @staticmethod
    def debug(text):
        if VERBOSE_LEVEL >= 2:
            log_line = Logger.__gen_log_line(u"DEBUG", text)
            Logger.__console_writer.debug(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

    @staticmethod
    def verbose(text):
        if VERBOSE_LEVEL >= 1:
            log_line = Logger.__gen_log_line(u"VERBOSE", text)
            Logger.__console_writer.verbose(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

    @staticmethod
    def info(text):
        log_line = Logger.__gen_log_line(u"INFO", text)
        Logger.__console_writer.normal(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

    @staticmethod
    def warn(text):
        log_line = Logger.__gen_log_line(u"WARN", text)
        Logger.__console_writer.warn(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

    @staticmethod
    def fatal(text, exitcode=-1):
        log_line = Logger.__gen_log_line(u"FATAL", text)
        Logger.__console_writer.error(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

        log_line = Logger.__gen_log_line(u"FATAL", (u"Program terminated, exit code: %d." % exitcode))
        Logger.__console_writer.error(log_line)
        if Logger.__file_logger:
            Logger.__file_logger.log(log_line)

        sys.exit(exitcode)

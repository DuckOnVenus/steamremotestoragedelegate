import BaseHTTPServer
import json
import os
import traceback
import urlparse
import time
from SocketServer import ThreadingMixIn
import threading
import sys
import locale
import requests

from lib.Logger.Logger import Logger

defaule_locale = locale.getdefaultlocale()[1]

__DEFAULT_CONFIG = {
    'api_key': 'F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0',
    'access_token': [],
    'max_single_query_limit': 100,
    'listen_address': '0.0.0.0',
    'listen_port': 27200,
    'https_pem_cert': './cert/fullchain.pem',
    'https_pem_pkey': './cert/privkey.pem',
    'https_enable': True,
    'save_log': True
}

CONFIG = {}

config_filename = 'proxy_config.json'


def save_config():
    with open(config_filename, 'w') as f:
        f.write(json.dumps(CONFIG, indent=4, sort_keys=True))


def load_config():
    global CONFIG
    if os.path.exists(config_filename):
        with open(config_filename) as json_file:
            json_data = json.load(json_file)
            for i, v in __DEFAULT_CONFIG.items():
                if i in json_data:
                    CONFIG[i] = json_data[i]
                else:
                    CONFIG[i] = __DEFAULT_CONFIG[i]
    else:
        CONFIG = __DEFAULT_CONFIG
    save_config()


class WebRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def __init__(self, request, client_address, server):
        self.access_tokens = CONFIG['access_token']
        self.max_single_query_limit = CONFIG['max_single_query_limit']
        BaseHTTPServer.BaseHTTPRequestHandler.__init__(self, request, client_address, server)

    def parse_request_param(self, params_str):
        query_dict = urlparse.parse_qs(params_str)

        if self.access_tokens and len(self.access_tokens) > 0:
            request_token = query_dict.get('atok', None)
            if (request_token is None) or (request_token[0] not in self.access_tokens):
                raise ValueError('Invalid accessing token')

        id_single = query_dict.get('publishedfileid', None)
        if id_single:
            return [id_single]
        else:
            item_counts = query_dict.get('itemcount', None)
            if item_counts is None:
                raise ValueError('missing required parameter: itemcount')
            item_counts = int(item_counts[0])
            if not (0 < item_counts <= self.max_single_query_limit):
                raise ValueError(
                    'the value of parameter "itemcount" exceed the maximum limit: %d' % self.max_single_query_limit)
            ids = []
            for i in range(item_counts):
                curr_id = query_dict.get('publishedfileids[%d]' % i, None)
                if curr_id is None:
                    continue
                curr_id = int(curr_id[0], base=10)
                ids.append(curr_id)
            return ids

    def handle_GetPublishedFileDetails(self, args):
        detailsUrl = 'https://api.steampowered.com/IPublishedFileService/GetDetails/v1/'

        try:
            mod_ids = self.parse_request_param(args)
        except ValueError as e:
            return 400, json.dumps(
                {'detail:': 'Bad request: %s' % str(e)}
                , sort_keys=False,
                encoding='utf-8')
        else:
            raw_args = [
                ('key', CONFIG['api_key'])
            ]

            for index, mod_id in enumerate(mod_ids):
                raw_args.append(('publishedfileids[%d]' % index, str(mod_id)))
            raw_args.append(('itemcount', str(len(mod_ids))))
            r = requests.get(detailsUrl, params=raw_args)
            return r.status_code, r.text

    def do_GET(self):
        ex_info = None
        try:
            parsed_path = urlparse.urlparse(self.path)
            request_path = parsed_path.path.strip('/')
            if request_path == 'ISteamRemoteStorage/GetPublishedFileDetails/V0001':
                code, body = self.handle_GetPublishedFileDetails(parsed_path.query)
            else:
                code, body = (404, "{'detail':'Not found'}")
        except Exception:
            Logger.warn(defaule_locale)
            code, body = 400, "{'detail:':'Bad request: internal error'}"
            ex_info = traceback.format_exc()

        Logger.info(
            u'Responded request "%s" from %s:%s with code %d%s' % (
                self.path, self.client_address[0], self.client_address[1], code,
                ex_info and (u', exception:\n%s' % ex_info) or u''
            )
        )

        message = '\r\n' + body

        self.send_response(code)
        self.end_headers()
        self.wfile.write(message.encode('utf-8'))


class ThreadingHttpServer(ThreadingMixIn, BaseHTTPServer.HTTPServer):
    pass


def main(argv):
    import ssl
    listen_address = CONFIG['listen_address']
    listen_port = CONFIG['listen_port']

    server = ThreadingHttpServer((listen_address, listen_port), WebRequestHandler)

    if CONFIG['https_enable']:
        cert_pem = CONFIG['https_pem_cert']
        pkey_pem = CONFIG['https_pem_pkey']
        server.socket = ssl.wrap_socket(server.socket, certfile=cert_pem, keyfile=pkey_pem,
                                        server_side=True)

    ip, port = server.server_address

    server_thread = threading.Thread(target=server.serve_forever)

    server_thread.setDaemon(True)
    server_thread.start()
    Logger.info('Server listening on %s:%s' % (ip, port))

    while True:
        time.sleep(1)


if __name__ == '__main__':
    load_config()
    Logger.init_logger(2, CONFIG['save_log'], 'Log', 'StatServer')
    main(sys.argv)
    save_config()
